<?php

namespace Wnowak\Userlog\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

class InstallSchema implements InstallSchemaInterface
{

    public function install(SchemaSetupInterface $setup,
                            ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $tableName = $installer->getTable('user_log');
        if ($installer->getConnection()->isTableExists($tableName) != true) {
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id', Table::TYPE_INTEGER, null,
                    [
                    'identity' => true,
                    'unsigned' => true,
                    'nullable' => false,
                    'primary' => true
                    ], 'ID'
                )
                ->addColumn(
                    'ip', Table::TYPE_TEXT, null,
                    ['nullable' => false, 'default' => ''], 'Ip'
                )
                ->addColumn(
                    'host', Table::TYPE_TEXT, null,
                    ['nullable' => false, 'default' => ''], 'Host'
                )
                ->addColumn(
                    'user_id', Table::TYPE_INTEGER, null, ['identity' => false],
                    'User id'
                )
                ->addColumn(
                    'date_login', Table::TYPE_DATETIME, null, ['nullable' => false],
                    'Login date'
                )
                ->addColumn(
                    'date_logout', Table::TYPE_DATETIME, null, ['nullable' => false],
                    'Logout date'
                )
                ->setComment('Logs user login')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}
<?php

namespace Wnowak\Userlog\Model;

use Magento\Cron\Exception;
use Magento\Framework\Model\AbstractModel;

/**
 * Contact Model
 *
 * @author      Wojciech Nowak
 */
class UserLog extends AbstractModel
{
    protected $id;
    protected $userId;
    protected $ip;
    protected $host;

    /**
     * @var \Magento\Framework\Stdlib\DateTime
     */
    protected $date;

    /**
     * @return void
     */
    protected function _construct()
    {
        $this->_init(\Userlog\Userlog\Model\ResourceModel\Contact::class);
    }
}